## CODE CHALLENGE CHAPTER 7

### Installation
**1. Clone the source code**
```
https://gitlab.com/cherdianto/challenge7_candra.git
```

**2. Install all dependencies**
```
$ npm Install
```

**3. Generate database**
```
$ sequelize db:create
```

**4. Run database migration**
```
$ sequelize db:migrate
```

**5. Run seeder for dummy data**
```
$ sequelize db:seed:all
```

**6. Start the server via nodemon**
```
$ npm run dev
```

### Routes for Game
**Register new account**
```
POST /api/v1/auth/register
```

**User/superuser login**
```
POST /api/v1/auth/login
```
DONT FORGET TO COPY THE TOKEN

**Create new game room**


COPY TOKEN FROM LOGIN RESPONSE, PUT IT AT HEADER AUTHORIZATION
```
POST /api/v1/auth/create-room
```
INSIDE THE REQUEST BODY
```
{
    "name" : <your_room_name>
}
```

**Join the room**
```
POST /api/v1/join-room/:roomid
```

**Play the game**
```
POST /api/v1/fight/:roomid
```

INSIDE THE REQUEST BODY 
```
{
    "pick" : <your_choice_B_or_K_or_G>
}
```


### Routes for API 
**All users data (superuser only)**

You must login as admin (username: admin, password: admin)
```
GET /api/v1/users
```

**Specific user data**
```
GET /api/v1/users/:id
```

**Room detail**
```
GET /api/v1/rooms/:id
```
